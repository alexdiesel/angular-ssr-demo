import {Component} from '@angular/core';

@Component({
  selector: 'app-dashboard-page',
  templateUrl: './dashboard.component.html',
})
export class DashboardPageComponent {

  sensors = [
    {title: 'Heat', params: 'Architecto consequuntur cumque explicabo maxime, officiis perspiciatis quae quos vitae.'},
    {title: 'Power', params: 'Yut culpa deleniti eaque eos fugiat, minus molestias nihil nisi quis ratione.'},
  ]

}
