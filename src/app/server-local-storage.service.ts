import {Injectable} from '@angular/core';

@Injectable()
export class ServerLocalStorageService {
  private store: { [key: string]: string } = {};

  setItem(key: string, value: string): void {
    this.store[key] = value;
  }

  getItem(key: string): string | null {
    return this.store[key];
  }
}
