import {Component} from '@angular/core';

@Component({
  selector: 'app-catalog-page',
  templateUrl: './catalog.component.html',
})
export class CatalogPageComponent {

  products = [
    {title: 'Consequuntur', desc: 'Consequuntur cumque explicabo maxime'},
    {title: 'Officiis', desc: 'Officiis perspiciatis quae quos vitae'},
    {title: 'Yut', desc: 'Yut culpa deleniti eaque eos fugiat'},
    {title: 'Minus', desc: 'Minus molestias nihil nisi quis ratione'},
    {title: 'Architecto', desc: 'Architecto consequuntur cumque'},
    {title: 'Deleniti', desc: 'Deleniti eaque eos fugiat'},
  ]

}
