import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {CatalogRoutingModule} from './catalog-routing.module';
import {CatalogPageComponent} from './pages/catalog/catalog.component';

@NgModule({
  imports: [
    CommonModule,
    CatalogRoutingModule,
  ],
  declarations: [
    CatalogPageComponent
  ]

})
export class CatalogModule {

}
