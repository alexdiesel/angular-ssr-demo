import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CatalogPageComponent} from './pages/catalog/catalog.component';


const routes: Routes = [
  {
    path: '', component: CatalogPageComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CatalogRoutingModule {

}
