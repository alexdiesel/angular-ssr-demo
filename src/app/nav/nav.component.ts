import {Component} from '@angular/core';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
})
export class NavComponent {

  liks = [
    {name: 'Home', link: '/'},
    {name: 'Catalog', link: '/catalog'},
    {name: 'Admin', link: '/admin'},
  ]

}
