import {Component, ViewEncapsulation} from '@angular/core';
import {LocalStorageService} from './local-storage.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AppComponent {

  constructor(private localStorage: LocalStorageService) {
    localStorage.setItem('name', 'mimiminja is here');
    console.log('localStorage', localStorage.getItem('name'));
  }
}
